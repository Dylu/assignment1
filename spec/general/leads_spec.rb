require 'spec_helper'

shared_examples "Create a new Lead" do |note_content|
  it "Open Add Lead form" do
    on(LeadsPage).add_new_lead_element.when_visible.click
  end
  
  it "Enter Lead's first name" do
    on(LeadsPage).lead_first_name_element.when_visible(10).value = lead_data[:first_name]
  end
  
  it "Enter Lead's last name" do
    on(LeadsPage).lead_last_name_element.when_visible(10).value = lead_data[:last_name]
  end
  
  it "Enter Lead's company name" do
    on(LeadsPage).lead_company_name_element.when_visible(10).value = lead_data[:company_name]
  end
  
  it "Enter Lead's title" do
    on(LeadsPage).lead_title_element.when_visible(10).value = lead_data[:title]
  end
  
  it "Enter Lead's email" do
    on(LeadsPage).lead_email_element.when_visible(10).value = lead_data[:email]
  end
  
  it "Enter Lead's mobile" do
    on(LeadsPage).lead_mobile_element.when_visible(10).value = lead_data[:mobile]
  end
  
  it "Enter Lead's phone" do
    on(LeadsPage).lead_phone_element.when_visible(10).value = lead_data[:phone]
  end
  
  it "Enter Lead's street" do
    on(LeadsPage).lead_street_element.when_visible(10).value = lead_data[:street]
  end
  
  it "Enter Lead's city" do
    on(LeadsPage).lead_city_element.when_visible(10).value = lead_data[:city]
  end
  
  it "Enter Lead's zip" do
    on(LeadsPage).lead_zip_element.when_visible(10).value = lead_data[:zip]
  end
  
  it "Enter Lead's region" do
    on(LeadsPage).lead_region_element.when_visible(10).value = lead_data[:region]
  end
  
  it "Save Lead" do
    on(LeadsPage).create_lead
  end
  
  it "Enter Lead's Note content" do
    @current_page.lead_note_content_element.when_visible(10).value = note_content
  end
  
  it "Save Note" do
    @current_page.create_lead_note
    sleep 1
  end
  
  it "Check if Lead's Note is visible on list" do
    lead_note = @current_page.get_lead_note note_content
    expect(lead_note[0]).not_to be_nil
  end
end

describe "Leads" do
  before(:all) do
    login_to_autotest
    visit(LeadsPage)
  end
  
  describe "Add new Lead" do
    include_examples "Create a new Lead", Faker::Lorem.sentence do
      let(:lead_data) {
        {
          first_name: 'Michael',
          last_name: 'Schumacher',
          company_name: 'Some Company Inc.',
          title: 'CEO',
          email: 'michaelschumacher@example.com',
          mobile: '123456789',
          phone: '987654321',
          street: '29 Red Lion Street',
          city: 'London',
          zip: 'WC1R 4PS',
          region: 'London'
        }
      } 
    end
  end
end
