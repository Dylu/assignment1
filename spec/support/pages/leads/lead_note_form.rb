module LeadNoteForm
  include PageObject
  
  text_area(:lead_note_content, :placeholder => "Add a note about this lead.")
  button(:create_lead_note, :value => "Save")
end