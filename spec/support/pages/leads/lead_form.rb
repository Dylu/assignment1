module LeadForm
  include PageObject
  
  div(:lead_form, :css => ".lead-new")
  text_field(:lead_first_name, :id => "lead-first-name")
  text_field(:lead_last_name, :id => "lead-last-name")
  text_field(:lead_company_name, :id => "lead-company-name")
  text_field(:lead_title, :id => "lead-title")
  text_field(:lead_email, :id => "lead-email")
  text_field(:lead_mobile, :id => "lead-mobile")
  text_field(:lead_phone, :id => "lead-phone")
  text_field(:lead_street, :id => "lead-street")
  text_field(:lead_city, :id => "lead-city")
  text_field(:lead_zip, :id => "lead-zip")
  text_field(:lead_region, :id => "lead-region")
  button(:create_lead, :value => "Save")
end