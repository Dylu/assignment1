class LeadsPage < AbstractPage
  include PageObject
  include LeadForm
  include LeadNoteForm
  include RelatedToPicker
  
  page_url "https://app.futuresimple.com/leads"
  
  link(:add_new_lead, :css => ".unselected")
  divs(:lead_notes_list, :css => ".item-body")
  
  def get_lead_note content
    lead_notes = []
    lead_notes_list_elements.each do |item|
      lead_notes << item if item.when_visible.text == content
    end
    lead_notes
  end
end